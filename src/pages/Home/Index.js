import React, { useState } from 'react';
import { View, Image, Text, TouchableOpacity, Alert } from 'react-native';

import api from '../../services/api';
import styles from './styles';

//import AsyncStorage from '@react-native-community/async-storage';

export default function Home({ navigation }) {
    const [wifi, setWifi] = useState('Schaun');
    const [localizacao, setLocaliza] = useState('Rua gonçalves chaves, 201');
    const [colaboradores_idcolaboradores, setId] = useState('');

    function doPonto() {


        const payload = {
            wifi,
            localizacao,
            colaboradores_idcolaboradores: window.idcolaboradores
        }
        api.post('/pontos', payload).then(({ data }) => {
            Alert.alert('Sucesso', ' OK! Ponto Registrado com sucesso!')
            navigation.navigate("Login")


        }).catch(err => alert('Erro no login'))

    }
    return (


        <View style={styles.container}>
            <View style={styles.info}>
            <Text style={styles.txtlocaliza}>Localização</Text>
                <View style={styles.viewTwo}>
                    <Text style={styles.txtWifii}> {localizacao}</Text>
                    <Image source={require('../../assets/localiza.png')} style={styles.logo} />
                </View>
                <View style={styles.viewlocaliza}>
                    <Text style={styles.txtWifi}>Wifi</Text>
                    <Text style={styles.txtWifii}>{wifi}</Text>
                </View>
            </View>

            <TouchableOpacity style={styles.btnSubmit} onPress={() => doPonto()}>
                <Text style={styles.textButtonS} >Registrar Ponto</Text>
            </TouchableOpacity>
        </View>
    )

}

