import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './pages/Login/Index';
import Home from './pages/Home/Index';

const Stack = createStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login"
          component={Login}
          options={{
            title: 'Hora Certa',
            headerTitleAlign: 'center',
            headerStyle: {
              backgroundColor: 'white'
            },
            headerTintColor: '#6AB4B9'
          }}
        />
        
        <Stack.Screen name="Home"
          component={Home}
          options={{
            title: 'Registro de Ponto',
            headerTitleAlign: 'center',
            headerStyle: {
              backgroundColor: 'white'
            },
            headerTintColor: '#6AB4B9'
          }}
        />
        
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
